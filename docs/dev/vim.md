---
id: vim
title: VI iMproved
slug: vim
tags:
    - tools
---


## Removing line
At `nano` we can run `CTRL+K` to remove a line.

`vim` do the same by entering command mode and issuing `dd`

You can also delete between:
```
:[start],[end]d
:1,5d
```
You can also use wildcard characters in the aforementioned command.

  - `.` Refers to the current line
  - `$` Denotes the end of file
  - `%` Matches all the lines in the file

Example
```
:.,5d #deletes lines between the current line and the fifth line.
:.,$d #removes all lines starting from the current line till the end.
:%d #clears the entire file
```

## Copy

To select, `CTRL+SHIFT+Left Click` then `CTRL+SHIFT+C` to copy to the clipboard.

You can also select by going to the end of the selection, entering into the view mode by pressing `v` and selecting the whole line with `END`.


## GPG encrypted files
You can edit GPG Encrypted files on-the-fly with vim.

1. Install the tool (vim-gnupg)
```
yay -S vim-gnupg
```

2. This way, you can edit gpg encrypted files with vim
```
vim <file.asc>
```

Simple, isn't it?


## References

  - [How to Delete One or More Lines in Vi and Vim](https://www.makeuseof.com/vim-vi-delete-line/)