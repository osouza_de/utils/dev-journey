---
id: jupyter
title: Jupyter Notebooks
slug: juoyter
tags:
    - code
    - dev
---
# Jupyter Notebooks

1. Install the Jupyter and Python extensions
``` bash
code --install-extension ms-toolsai.jupyter --install-extension ms-python.python
```

2. Even running with Docker remote kernel I got trouble. So it may be needed to install python on your local host machine.


## Notebook python local imports
While working with python kernel you may face a trouble using local imports, as the `%PWD` changes for remote kernel (running notebooks on vscode) or wven with the web ui.

So the solution below may help you with that.

Just create a cell with the content:
```python
# jupyter notebook local import stuff
%cd /src/notebooks
import sys
sys.path.append('../azure-functions')
```
1. `%cd /src/notebooks` change the working directory according to your needs.
2. Replace `sys.path.append('../azure-functions')` to the directory you need.


## Remote Docker kernel
You can run Jupyter notebooks on VSCode through a Docker Jupyter kernel.

1. Start the Docker Jupyter kernel and get the connection URL
```
[user@host srccode]$ docker-compose -f docker-compose.yml up
Starting notebooks-exc ... done
Attaching to notebooks-exc
notebooks-exc    | [I 14:10:08.629 NotebookApp] Serving notebooks from local directory: /src
notebooks-exc    | [I 14:10:08.629 NotebookApp] Jupyter Notebook 6.5.2 is running at:
notebooks-exc    | [I 14:10:08.629 NotebookApp] http://c892b52ac01b:8888/?token=33f97c14c78f3dd51bddafd5096e37c5ed2726a5b9120e3b
notebooks-exc    | [I 14:10:08.629 NotebookApp]  or http://127.0.0.1:8888/?token=33f97c14c78f3dd51bddafd5096e37c5ed2726a5b9120e3b
notebooks-exc    | [I 14:10:08.629 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
notebooks-exc    | [C 14:10:08.634 NotebookApp]
notebooks-exc    |
notebooks-exc    |     To access the notebook, open this file in a browser:
notebooks-exc    |         file:///root/.local/share/jupyter/runtime/nbserver-7-open.html
notebooks-exc    |     Or copy and paste one of these URLs:`
notebooks-exc    |         http://c892b52ac01b:8888/?token=33f97c14c78f3dd51bddafd5096e37c5ed2726a5b9120e3b
notebooks-exc    |      or http://127.0.0.1:8888/?token=33f97c14c78f3dd51bddafd5096e37c5ed2726a5b9120e3b
```

2. Open the Jupyter notebook you want to run

3. Open VSCode command pallete and search

Then

3.1 "Jupyter - Specify Jupyter Server for Connections"

3.2 Select "Existing"

3.3 Paste the connection URL from step 1

3.4 Set a name or just hit "Enter" to use the URL


4. Select the interpreter

You can do it byt clicking on "Kernel" (on top of the notebook) or via the command pallete "Notebook: Select Notebook Kernel"

Just pick one from the list.

And that's it!



## VSCode Settings:
CTRL + SHIFT + P > "Open User Settings (JSON)"
```json
    "jupyter.askForKernelRestart": true,
    "notebook.lineNumbers": "on",
    "notebook.insertToolbarLocation": "notebookToolbar",
    "notebook.experimental.outputScrolling": true,
    "notebook.output.textLineLimit": 10,
    "jupyter.disableJupyterAutoStart": true,
```


## Extras

### Jupyter magic tricks
Here's an [article](https://www.dominodatalab.com/blog/lesser-known-ways-of-using-notebooks) where you can check more cool stuff.


```
# Magics
% cd /src

# Shell command
! cd /src
```


### Installing an package on jupyter without confirmation prompt
For instance, you may install python packages, run bash or any other executable on the jupyter interpreter host:

```bash
## Installing netcat without user prompt confirmation
!apt -y install netcat
```

### Jupyter error logs
Settings > Jupyter > Logging: Level = everything

```json
"jupyter.logging.level": "everything"
```

Then, it will be the output from "Jupyter" console tab

Output > "Jupyter"


## 404 Bug on WSL VSCode connecting to Docker Jupyter.

1. When selecting the remote notebook, don't set the full url.

Instead: `http://127.0.0.1:8888/tree`

2. and then put the 'token' as the password at the second prompt.

