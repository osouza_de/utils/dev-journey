---
id: url
title: JS URL handling
slug: url
tags:
    - code_snippet
    - javascript
---

## URL Info Access

JavaScript provides you with many methods to retrieve and change the current URL, which is displayed in the browser's address bar. All these methods use the Location object, which is a property of the Window object. You can read the current Location object by reading `window.location`:

```javascript
var currentLocation = window.location;
```

### Basic URL Structure
`<protocol>//<hostname>:<port>/<pathname><search><hash>`

  - `protocol`: Specifies the protocol name be used to access the resource on the Internet. (HTTP (without SSL) or HTTPS (with SSL))

  - `hostname`: Host name specifies the host that owns the resource. For example, www.stackoverflow.com. A server provides services using the name of the host.

  - `port`: A port number used to recognize a specific process to which an Internet or other network message is to be forwarded when it arrives at a server.

  - `pathname`: The path gives info about the specific resource within the host that the Web client wants to access. For example, /index.html.

  - `search`: A query string follows the path component, and provides a string of information that the resource can utilize for some purpose (for example, as parameters for a search or as data to be processed).

  - `hash`: The anchor portion of a URL, includes the hash sign (#).

With these Location object properties you can access all of these URL components and what they can set or return:

  - **href** - the entire URL
  - **protocol** - the protocol of the URL
  - **host** - the hostname and port of the URL
  - **hostname** - the hostname of the URL
  - **port** - the port number the server uses for the URL
  - **pathname** - the path name of the URL
  - **search** - the query portion of the URL
  - **hash** - the anchor portion of the URL
  - **origin** - the window.location.protocol + '//' + window.location.host


## Example
```javascript
console.log(window.location.pathname.replace("/", "").split("/"));
console.log("path: " + window.location.pathname);
console.log("search: " + window.location.search);
console.log("hash: " + window.location.hash);

// Accessing https://devjourney.osouza.de/blog/hello-world/?q=test&i=1#childs-play
// Produces:

(3) ['blog', 'hello-world', '']
path: /blog/hello-world/
search: ?q=test&i=1
hash: #childs-play
````


## References

  - [The Red Pea @ StackOverflow](https://stackoverflow.com/a/20746566)