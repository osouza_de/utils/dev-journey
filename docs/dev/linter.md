---
id: linter
title: Linter
slug: linter
tags:
    - code
    - dev
---

We should always keep a code standard. And, when coding in a team, it would be nice to "ensure it"

One can achieve that by setting **code linters**

