---
sidebar_position: 5
title: Glossary
slug: glossary
---

# Glossary

List of some commom terms so you don't get lost...


## A

### API
**A**pplication **P**rogramming **I**nterface


## B


## C

### CDN
**C**ontent **D**elivery **N**etwork

### CLI
**C**ommand **L**ine **I**nterface

### CMS
**C**ontent **M**anagement **S**ystem

### CRUD
**C**reate, **R**ead, **U**pdate and **D**elete


## D

### DNS
**D**omain **N**ame **S**ystem

### DOM
**D**ocument **O**bject **M**odel

## E

### ES
**E**cma **S**cript

## F


## G


## H

### HODL [*](https://www.investopedia.com/terms/h/hodl.asp)
**H**old **O**n for **D**ear **L**ife

### HTTP
**H**yper **T**ext **T**ransfer **P**rotocol


## I

### IDE
**I**ntegrated **D**evelopment **E**nvironment

### IOC
**I**ndicator **O**f **C**ompromise


### IP
**I**nternet **P**rotocol

## J

### JSON
**J**ava**S**cript **O**bject **N**otation

## K

## L

## M

### MVC
**M**odel **V**iew **C**ontroller

## N

## O

### OOP
**O**bject **O**riented **P**rogramming



## P
### PWA
**P**rogressive **W**eb **A**pplication

## Q

## R

### REGEX
**REG**ular **EX**pression

### REST
**RE**presentational **S**tate **T**ransfer


## S

### SEO
**S**earch **E**ngine **O**ptimization

### SDK
**S**oftware **D**evelopment **K**it

### SOC
**S**ecurity **O**perations **C**enter

### Stack
A stack can be explained as a set of tools, including languages, frameworks, and other technologies that a developer uses to do their job.


## T

## U

### UI
**U**ser **I**nterface

### UX
**U**ser e**X**perience

## V

## W

### WWW
**W**orld **W**ide **W**eb

## X

### XML
e**X**tensible **M**arkup **L**anguage

## Y

## Z