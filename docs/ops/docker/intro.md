---
sidebar_position: 1
id: intro
title: Intro
tags:
    - docker
    - basic
---
# Docker Intro

First, make sure to install and configure Docker in your system:
  - [Archlinux](../linux/arch#base-packages-and-aur)



Remove all the containers
```
docker rm $(docker ps -aq)
```
