---
id: flashing
title: Flashing
slug: flashing
tags:
    - openwrt
---
https://openwrt.org/toh/hwdata/tp-link/tp-link_archer_c20_ac750_v4

It was

Firmware Version 3.16.0 0.9.1 v600f.0 Build 180124 Rel.58224n
Hardware Version Archer C20 v4 00000004

I read something that says TFTPD is needed to upgrade this router's firmware

https://openwrt.org/toh/tp-link/archer_c20_v4#using_tftp_method

Using TFTP method
→ generic.flashing.tftp

On Linux:
Download the …squashfs-tftp-recovery.bin as shown in the table above.
Configure a computer with static IP 192.168.0.66/24 and a TFTP server. On Debian or Ubuntu you can use either the tftpd-hpa or tftpd server packages.
Rename the downloaded firmware file to tp_recovery.bin and place it in the tftp server's root directory. (If using tftpd-hpa this is /var/lib/tftpboot/; if tftpd, it is /srv/tftp/.) You can test that the file is downloadable with tftp localhost and get tp_recovery.bin.
Connect the computer to one of the router's Ethernet ports while the router is off. Press and keep pressed the router's reset button and power it up. After about 7-10 seconds release the reset button. The power LED will flicker rapidly for ~3 seconds, indicating download of the firmware file.
The router will write the firmware to flash during ~40 more seconds of occasional power LED blinks, and then will reboot by itself, ready for use.


Stopping DHCP
# systemctl stop dhcpcd.service

# nano /etc/netctl/enp3s0

Description='A basic static ethernet connection'
Interface=enp3s0
Connection=ethernet
IP=static
Address='192.168.0.66/24'
Gateway='192.168.0.1'
DNS=('192.168.0.1')
TimeoutOp=10
SkipNoCarrier=yes
>>>EOF


Remove interfaces IP
# ip address flush dev enp3s0

# ifconfig enp3s0 down
# systemctl disable dhcpcd
# netctl start enp3s0


### TFTPD
# pacman -S tftp-hpa
# systemctl start tftpd
# cp ~/Dev/router/openwrt-21.02.0-ramips-mt76x8-tplink_archer-c20-v4-squashfs-tftp-recovery.bin /srv/tftp/tp_recovery.bin


UNDOING
# netctl disable enp3s0
# netctl stop enp3s0
# systemctl stop tftpd

