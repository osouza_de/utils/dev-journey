---
id: setup
title: Setup
slug: setup
tags:
    - openwrt
---

## Basic setup

0. Login via ssh with `root` and no password.
```
ssh root@192.168.1.1
```

1. Change hostname and timezone.
```
uci set system.@system[0].hostname=<yourhostname>
uci set system.@system[0].timezone=UTC-3
uci commit
```

2. Set root password
```
passwd
```

3. Update and install some packages

```
opkg update
opkg install zerotier shadow-useradd shadow-su
```

4. Create a new user

Create a user and set a password
```
usr=and
useradd $usr
passwd $usr
```

Home directory setup
```
mkdir /home
mkdir /home/$usr
chown $usr /home/$usr
```

SSH key access

Create user's SSH dir and setup authorized keys

```
mkdir /home/$usr/.ssh
vim /home/$usr/.ssh/authorized_keys
```
Now, inside the authorized_keys file, you should add your SSH pubkeys.


Final user account setup
```
echo "${usr}:x:1000:1000:${usr}:/home/${usr}:/bin/ash" >> /etc/passwd
```


## SSH

1. Edit dropbear's config file

```
vim /etc/config/dropbear
```

```
config dropbear
        option PasswordAuth     'off'
        option RootPasswordAuth 'off'
        option RootLogin        'no'
        option Port             '5522'
#       option BannerFile       '/etc/banner'
```


## ZeroTier

1. Edit the zerotier config file
```
vim /etc/config/zerotier
```

```
config zerotier 'sample_config'
	option enabled '1'
	list join 'hahahahaha'
```
  - `option enabled` set to '1'
  - `list join` is your network id

2. Also, set the firewall rules
```
vim /etc/config/firewall
```

```
# Zerotier rules
config zone 'vpn_zone'
	option name 'zerotier'
	option input 'ACCEPT'
	option forward 'REJECT'
	option output 'ACCEPT'
	option device 'zt+'
	option masq '1'
	option mtu_fix '1'

config forwarding
	option dest 'zerotier'
	option src 'lan'

config forwarding
	option dest 'lan'
	option src 'zerotier'
```

3. Now, restart firewall and ZeroTier services
```
/etc/init.d/firewall restart
/etc/init.d/zerotier restart
```

Don't forget to authorize this new client into your zerotier network.

## WiFi

TODO

1. To see the available devices
```
uci show wireless

wireless.radio0=wifi-device
wireless.radio0.type='mac80211'
wireless.radio0.path='platform/10300000.wmac'
wireless.radio0.channel='1'
wireless.radio0.band='2g'
...
wireless.radio1=wifi-device
wireless.radio1.type='mac80211'
wireless.radio1.path='pci0000:00/0000:00:00.0/0000:01:00.0'
wireless.radio1.channel='36'
wireless.radio1.band='5g'
wireless.radio1.htmode='VHT80'
wireless.radio1.disabled='1'
...
```


uci set wireless.radio0.country='BR'
uci set wireless.radio1.country='BR'

uci set wireless.radio0.disabled='0'
uci set wireless.radio1.disabled='0'

uci commit wireless

wifi reload

## References

  - [Set up a ZeroTier network on OpenWRT](https://kysonlok.github.io/2020/06/12/Set-Up-a-ZeroTier-Network-on-OpenWRT/)
  - [Securing OpenWRT](https://openwrt.org/docs/guide-user/security/secure.access#create_a_non-privileged_user_in_openwrt)