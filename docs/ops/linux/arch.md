---
sidebar_position: 1
id: arch
title: Archlinux
slug: arch
tags:
    - linux
---

# Arch Linux (Manjaro, please)

## Installing manjaro zsh config
```
yay -S manjaro-zsh-config
```

The final setup says:
"""
Add the content of /etc/zsh/zshrc-manjaro into ~/.zshrc to activate.
"""

 ttf-meslo-nerd-font-powerlevel10k

## Default shell

Manjaro has a nice shell, make use of it:

```
sudo chsh -s /bin/zsh $USER
```

## Initial setup

**Enable multilib!**
As this [ArchLinux wiki - Repositories#Multilib](https://wiki.archlinux.org/title/Official_repositories#Enabling_multilib) guide shows.

Uncomment the "[multilib]" section in the `/etc/pacman.conf` and update the packages with `sudo pacman -Syyu`


0. Configure pacman to use simultaneous downloads :)

Edit `/etc/pacman.conf` line "ParallelDownloads" to something like 5
```
sudo sed -i -e "s/.*ParallelDownloads.*/ParallelDownloads = 5/" /etc/pacman.conf
```

1. System update
First, make sure to update your system's keys and packages

```
pacman-key --init
pacman-key --populate
pacman-key --refresh-keys
pacman -Sy archlinux-keyring
pacman -Syyu
```

2. Base packages
Then, install some basic packages

```
pacman -S base-devel docker docker-compose git glances jq man netcat net-tools openssh vim zsh
```

2.1 The following may be usefull in a GUI env
```
pacman -S discord terminator vscode
```

3. User setup
Set up a user and quit root

3.1 Create a sudo group
```
groupadd sudo
```

3.2 Enable sudoers file
You can open the file `/etc/sudoers` and uncomment the lines `# %wheel ALL=(ALL:ALL) ALL` and `# %sudo ALL=(ALL:ALL) ALL`. Or just run the commands:


Allow members of group wheel to execute any command
```
sed -i -e "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/" /etc/sudoers
```
Or `%wheel ALL=(ALL) NOPASSWD: ALL`


Allow members of group sudo to execute any command
```
sed -i -e "s/# %sudo ALL=(ALL:ALL) ALL/%sudo ALL=(ALL:ALL) ALL/" /etc/sudoers
```

3.3 User group
Add the user to the sudoers group

```
useradd -m -G wheel,sudo -s /bin/bash <username>
```

3.4 User password
Set a password to the user

```
passwd <user>
```


3.5 Directories (**WSL only**)

```
cp .ssh ~/ -R
cp .gitconfig ~/ -R
cp .gnupg ~/ -R
```

And fix ssh dir permissions
```
chmod 0644 ~/.ssh/id_*.pub && chmod 0600 ~/.ssh/id_*
```

Don't forget to [configure the SSH Key Agent](../../sec/encryption/ssh/key#key-agent)


3.6 Default user (WSL only)

Set the default user to arch wsl

Add the following content to `/etc/wsl.conf`
```
[user]
default = <username>
```

Then, terminate your WSL instance to apply the settings.

4. yay AUR
Now, let's configure "yay" package manager to install AUR packages

```
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si
```

5. AUR packages
Install some usefull AUR packages

```
yay -S cmatrix minikube vim-gnupg
```

5.1 GUI ones
```
yay -S drawio-desktop google-chrome teamviewer tor-browser
```

## Flutter
```
# Configure Snap
sudo ln -s /var/lib/snapd/snap /snap

# Install flutter via snap
sudo snap install flutter --classic

# Install android studio and sdk stuff
yay -S android-studio android-sdk-cmdline-tools-latest

# Then, run flutter-doctor to check
flutter doctor
```

If you installed `google-chrome-stable`, there'll be a "GOOGLE CHROME not found error". Fix it:

```
sudo ln -s /usr/bin/google-chrome-stable /usr/bin/google-chrome
``` 
Other error that you'll face is 'unable to locate Android SDK'.
First of all, go ahead to:
```
https://developer.android.com/studio/index.html

Download Android Studio

``` 
Go to the directory where you downloaded the Android Studio
```
extract the file

open your terminal:
sudo mv ~/Downloads/android-studio /usr/local -> it's a example, use the correct path that you extracted android-studio
cd /usr/local/android-studio/bin
/usr/local/android-studio/bin$ ./studio.sh
```
Then open your Android Studio from menu and install the IDE. 

You'll see a screen with 'project' 'new project' 'more info' click in more info and open SDK Manager

Under SDK Location there is three SDK sessions, select SDK Tools
```
Install: 
Android SDK Build-Tools 33-rc3
Android SDK Command-line tools (latest)
Android Emulator
Android SDK Plataform-Tools

apply and accept the confirmation to download the packages
```
Run flutter doctor --android-licenses
Accept all
And voilà!

## Docker

If you followed the [guide above](#base-packages-and-aur), you'll already have Docker installed in your system.

But now you should configure it properly to autostart on every boot.

```
# Enable the docker service to start now and with the system
sudo systemctl enable --start docker
```

### Docker group
```
sudo usermod -aG docker $USER
```

### Docker sudo
I don't recommend running docker with a normal user (adding it to the docker user group). But I know that typing "sudo docker" everytime is a pain in the 4$$...

So... set a docker command alias to sudo!
```
# The following example works with ZSH! Please, note if you're using a different shell...
echo "alias docker='sudo docker'" >> ~/.zshrc
echo "alias docker-compose='sudo docker-compose'" >> ~/.zshrc
```
**Note** I had to follow the "docker group" approach to make kubernetes work :(


## Kubernetes

1. Install Kubernetes packages and it's dependencies
```
pacman -S helm kubeadm kubectl kubelet libvirt libvirt qemu-headless ebtables dnsmasq
```

2. Setup
```
## Add your user to 'libvirt' group (also docker if not yet)
sudo usermod -aG libvirt $USER

## Enable and start 'libvirtd' service
sudo systemctl enable --now libvirtd.service

## Set kvm2 driver as default
minikube config set driver kvm2
```

3. Start
```
minikube start

## OR you can also start minikube with a custom drive, set at runtine:
minikube start --driver=docker
```


## Extras

### GPG Key Agent

1. Create a gpg-agent config file with the command and content:

```
vim ~/.gnupg/gpg-agent.conf
```

```
default-cache-ttl 34560000
max-cache-ttl 34560000
pinentry-program /usr/bin/pinentry-curses
```

2. Append the following to `~/.bashrc`

```
# enable GPG signing
export GPG_TTY=$(tty)

if [ ! -f ~/.gnupg/S.gpg-agent ]; then
    eval $( gpg-agent --daemon --options ~/.gnupg/gpg-agent.conf )
fi

export GPG_AGENT_INFO=${HOME}/.gnupg/S.gpg-agent:0:1
```

As seen [here](https://www.jessesquire.com/articles/2019/03/31/configure-github-activity-signing-with-wsl/)


### SSH Key Agent

1. Add the following piece of code to your shell init script (for example: ~/.bashrc)

```
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi
```

1.1 If you are on WSL, use the following code
```
# Set up ssh-agent
SSH_ENV="$HOME/.ssh/environment"

function start_agent {
    echo "Initializing new SSH agent..."
    touch $SSH_ENV
    chmod 600 "${SSH_ENV}"
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' >> "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    #/usr/bin/ssh-add
    # The line above was commented to avoide asking for SSH Key at login
}

# Source SSH settings, if applicable
if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    kill -0 $SSH_AGENT_PID 2>/dev/null || {
        start_agent
    }
else
    start_agent
fi
```
Taken from [this guide](https://github.com/abergs/ubuntuonwindows#2-start-an-bash-ssh-agent-on-launch)

2. Now, set the SSH Agent  at  ~/.ssh/config

```
AddKeysToAgent yes
```

### SSH

Now it's time to harden our system even more.

1. Let's remove SSH password login, accept SSH key only, and change the SSH Server port.
```
nano /etc/ssh/sshd_config
```

```
Port 5522
ListenAddress 0.0.0.0
LogLevel INFO
PermitRootLogin no
PubkeyAuthentication yes
AuthorizedKeysFile %h/.ssh/authorized_keys
PasswordAuthentication no
AllowAgentForwarding yes
X11Forwarding yes
PrintLastLog yes
```

2. Enable and start the service
```
systemctl enable --now sshd
```


### ZeroTier

1. Install
```
yay -S zerotier-one
```

2. Enable and start the service
```
systemctl enable --now zerotier-one
```

3. Join network
```
zerotier-cli join <networkid>

200 join OK
```

4. Go the the network management panel and authorize the host: `https://my.zerotier.com/network/<networkid>`

Also assign an IP address.

5. Check status
```
zerotier-cli info
200 info <hostid> 1.8.10 ONLINE

zerotier-cli listnetworks
200 listnetworks <nwid> <name> <mac> <status> <type> <dev> <ZT assigned ips>
...
```


### Turnoff screen
```
xset dpms force off
```

## References

  - [ArchWiki - SSHKeys](https://wiki.archlinux.org/title/SSH_keys#SSH_agents)
  - [ld100 - Migrating from Ubuntu on WSL to ArchLinux on WSL2](https://gist.github.com/ld100/3376435a4bb62ca0906b0cff9de4f94b)