---
id: img_mount
title: Mounting img files
slug: linux/img_mount
tags:
    - linux
    - misc
---


We'll see how to mount a `.img` file in order to access it from your host machine.


1. Get the image's partition layout:

```
fdisk -lu retropie01-05-2020.img

Disk retropie01-05-2020.img: 7,4 GiB, 7948206080 bytes, 15523840 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xd42bc226

Device                  Boot  Start      End  Sectors  Size Id Type
retropie01-05-2020.img1 *      8192   124927   116736   57M  e W95 FAT16 (LBA)
retropie01-05-2020.img2      124928 15523839 15398912  7,3G 83 Linux
```


2. Calculate the offset you want to mount times block size

In this case, we don't want to mount `/boot`, so let's skip it and mount only the root filesystem.

```
124928 * 512 = 63963136
```


3. Mount the img to loop using the offset

```
losetup -o 63963136 /dev/loop0 retropie01-05-2020.img
```


4. Mount with:

```
mount /dev/loop0 /mnt/rasp
```


5. To umount, use the following command

```
umount /mnt/rasp
losetup -d /dev/loop0
```