---
id: kali
title: Kali Linux
slug: raspberrypi/kali
tags:
    - infosec
    - kali
    - raspberrypi
---

Some basic things you should do right after flashing your Raspberry Pi Kali Linux. (Yeah, I know that Ansible could automate this...)

1. First of all, I like to change the hostname. I usually use eth0 interface MAC address' first and last portions.
```
ifconfig eth0 | grep ether | awk '{split($2, mc, ":"); print mc[1] mc[6]}'
```

```
sudo su
host='yourhostname'
echo $host > /etc/hostname
```

You also should add the entry at `/etc/hosts` (remember to remove the old one)
```
nano /etc/hosts
```

```
127.0.0.1      yourhostname
...
```

2. Disable GUI login

See which method are in use.
```
systemctl get-default && if [ $(systemctl get-default | grep 'graphical.target') ]; then echo "GUI"; else echo "CLI"; fi
```
If `graphical.target`, then GUI mode is active.

So you can set to CLI with
```
systemctl set-default multi-user.target
```


## Credentials

1. Create a password for `root`
```
sudo su
passwd

New password: 
Retype new password: 
passwd: password updated successfully
```

2. Change `kali` user default password
```
passwd kali

New password: 
Retype new password: 
passwd: password updated successfully
```


## Updating and installing packages

1. Make sure to update the repository list and the packages themselves.

**Note:** this step may take a while to finish.
```
apt update

apt upgrade -y

reboot now
```

2. Install some useful tools.
```
apt install -y asleap beef bettercap hcxdumptool hcxtools hostapd hostapd-wpe isc-dhcp-server lighttpd mdk4 terminator
```


## WiFi

1. Set a hidden wifi hotspot on your mobile device

2. Create a file at `/etc/NetworkManager/system-connections/<200e>.nmconnection`:
```
[connection]
id=‎
uuid=f23ca401-d48b-4ed4-b3e0-5095581892f0
type=wifi
interface-name=wlan0

[wifi]
hidden=true
mode=infrastructure
ssid=226;128;142;

[wifi-security]
auth-alg=open
key-mgmt=wpa-psk
psk=<password>

[ipv4]
method=auto

[ipv6]
addr-gen-mode=stable-privacy
method=auto

[proxy]
```

Note: the following command connects to the wifi even hidden, but not with a empty char SSID:
```
nmcli device wifi connect 'SSID' password 'password' ifname wlan0 hidden yes
```


## SSH
Now it's time to harden our system even more.

Let's remove SSH password login, accept SSH key only, and change the SSH Server port.
```
nano /etc/ssh/sshd_config
```

```
Port 5522
ListenAddress 0.0.0.0
LogLevel INFO
PermitRootLogin no
PubkeyAuthentication yes
AuthorizedKeysFile %h/.ssh/authorized_keys
PasswordAuthentication no
AllowAgentForwarding yes
X11Forwarding yes
PrintLastLog yes
```

I think that is a good security to your server.


### SSH Agent forwarding
With SSH Agent forwarding you can leverage using your host's keys at the connected machine. Make sure you [properly set up SSH Agent](../../sec/encryption/ssh_key.md#key-agent) in your host.

Now, you can use SSH with the `-A` option to use Agent forwarding:
```
ssh -A user@host
```

Of course, you can configure it in a way that you will not have to set the `-A` flag every time. However, **I do not recommend it for security reasons.**
```
nano ~/.ssh/config
```

```
# If you want to forward SSH Agent to every connection, set it at the top
ForwardAgent yes

# If you prefer to choose each host:
Host example
    ForwardAgent yes

# Alternatively, if you allow it globally but want to deny it to some hosts:
Host example-deny
    ForwardAgent no
```

You can test if it's working by running the following command at the CONNECTED machine
```
echo "The SSH Agent forwarding is$([ -z $SSH_AUTH_SOCK ] && echo " NOT") working."
```

That's it. Now you can use every key that is laid on your host's SSH key agent.


## ZeroTier
ZeroTier allows us to create a online virtual network.

1. Install it
```
curl -s https://install.zerotier.com | sudo bash

...
*** Waiting for identity generation...

*** Success! You are ZeroTier address [ <your machine address> ].
```

2. Enable ZeroTier service
```
systemctl enable zerotier-one
```

3. Join the network:
```
zerotier-cli join <networkid>
```

4. Login into your zerotier network configuration page and authorize the just added host: `https://my.zerotier.com/network/<networkid>`

5. Check status
```
zerotier-cli info
200 info 86b83d3a75 1.8.10 ONLINE

zerotier-cli listnetworks
200 listnetworks <nwid> <name> <mac> <status> <type> <dev> <ZT assigned ips>
...
```

Done.

## WiFi Management
Be sure to check [RaspberryPi - WiFi Management](wifi_management.md) in order to be able to reach your system by it's own wifi network.


## GPG and SSH keys
Also, setup [SSH](../../sec/encryption/ssh_key.md#generating) and [GPG](../../sec/encryption/gpg_key.md#generating) keys for your host.


## References

  - [What is SSH Agent Forwarding and How Do You Use It?](https://www.howtogeek.com/devops/what-is-ssh-agent-forwarding-and-how-do-you-use-it/)
  - [ZeroTier](https://www.zerotier.com/)
  - [ArchWiki - NetworkManager](https://wiki.archlinux.org/title/NetworkManager)