# Windows Tweaks

  - `Win + I`: Settings shortcut

## Random MAC
It is a nice idea to randomize your WiFi MAC address.

Got to `Settings > Network & internet > Wi-Fi` and choose `Random hardware addresses`

Into the `Manage known networks`, you can handle this setting for each network.



## References

  - [MS Learn - How to use random hardware addresses in Windows](https://support.microsoft.com/en-us/windows/how-to-use-random-hardware-addresses-in-windows-ac58de34-35fc-31ff-c650-823fc48eb1bc)