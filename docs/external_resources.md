---
title: External Resources
tags:
    - resources
---

A list with some useful resources for daily use.

## QuillBot - Grammar
Besides the grammar checker, it has plagiarism detection, a summarizer and other features.
- https://quillbot.com/grammar-check

## Language Tool
Multilingual grammar, style, and spell checker.
- https://languagetool.org/

## Google Translator
Self-explanatory.
- https://translate.google.com

## MD Emojis
This emoji list can be used in markdown files.
- https://gist.github.com/rxaviers/7360908
