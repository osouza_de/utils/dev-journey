---
title: Email
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---


# Checking e-mail events

```kql
EmailEvents
// | where RecipientEmailAddress == ""
// | where SenderFromAddress == ""
// | where NetworkMessageId == ""
| where TimeGenerated > ago(5d)
```


You can also search for the messageId:

```kql
search ""
| where $table !in ("")
| where $table in("UrlClickEvents", "EmailEvents", "EmailUrlInfo", "EmailAttachmentInfo", "EmailPostDeliveryEvents")
//| where TimeGenerated between(datetime("2023-01-01 00:00:00") .. now())
| where TimeGenerated > ago(2d)
| order by TimeGenerated asc
```

Finding the URLs
```kql
EmailUrlInfo
| where NetworkMessageId == ""
| where TimeGenerated > ago(5d)
| order by TimeGenerated asc
| summarize arg_max(UrlLocation, UrlDomain, *) by Url
```