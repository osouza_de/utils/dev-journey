---
title: Utils
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---

## Multi-workspace
Choose the table you want to search on and then, add the workspaces.
```
union SecurityIncident,
workspace("").SecurityIncident,
// rest of the query...
```


## Reordering fields

Syntax
```
| summarize arg_max(TimeGenerated, <other fields to reorder>, *) by <FieldToBeUnique>
```

Example with SecurityIncident
```
SecurityIncident
| where TimeGenerated > ago(180d)
| where Status == "Closed"
| summarize arg_max(TimeGenerated, Severity, ClassificationComment, Classification, Owner["assignedTo"], Title, *) by IncidentName
| order by TimeGenerated desc
```

Placing "TimeGenerated" on front
```
| summarize arg_max(TimeGenerated, *) by 
```

What's the diff?
```
| summarize arg_max(TimeGenerated, *) by SystemAlertId
| summarize TimeGenerated=any(TimeGenerated, *) by SystemAlertId
```

## Searching

It is very usefull during investigation.

1. Table exclusion
To exclude a Table while searchin for something...
```
search "search string"
| where $table <> "AlertEvidence"
| where $table !in ("BehaviorAnalytics", "SecurityEvent", "Event")
```

2. Period

2.1 To search from past time entries:
```
| where TimeGenerated > ago(7d)
```

2.2 Filtering between dates
```
| where TimeGenerated between(datetime("2023-01-01 00:00:00") .. now())
```

You can use `d` for days, `m` for months...


### Searching in specific tables
```
search in (Table, Table2) "Term"
```

### Example query...
```
search ""
| where $table !in ("")
//| where TimeGenerated between ((todatetime('2023-05-12T11:07:37Z') - 10m) .. (todatetime('2023-05-13T11:00:00Z') + 20m))
| where TimeGenerated > ago(2d)
| order by TimeGenerated asc
```


## References

  - [MS Learn: Optimize log queries in Azure Monitor](https://learn.microsoft.com/en-us/azure/azure-monitor/logs/query-optimization)