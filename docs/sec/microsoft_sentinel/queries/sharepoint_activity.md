---
title: Sharepoint / Office Activity
tags:
    - infosec
    - SOC
    - blue team
    - ms sentinel
---

"Monitor Site Collection Administrator" incidents

Base query
```kql
OfficeActivity
| where Operation == "SiteCollectionAdminAdded"
| where UserId has "user.name"
// we can search for the sharepoint site
//| where Site_Url has "https://company.sharepoint.com/sites/SITE-Id-lorem-ipsum"
// or for the IP
//| where ClientIP == ""
| order by TimeGenerated asc
//| project TimeGenerated, UserId, Site_Url, ModifiedProperties
```

Properties that we observe:
- UserId      app@sharepoint
- Site_Url    https://company.sharepoint.com/sites/SITE-Id-lorem-ipsum
- ModifiedProperties
  - Name  SiteAdmin
  - NewValue  user@email.com


## Resumed query returns
```
| project TimeGenerated, RecordType, Operation, ModifiedProperties, Site_Url
```


# Mass download for a single user

Getting activity related to SharePoint file downloads

```kql
OfficeActivity
| where EventSource == "SharePoint" and Operation == "FileDownloaded"
| where UserId has "user.name"
| order by TimeGenerated asc
| summarize count() by Site_Url, UserId, ClientIP
//| project TimeGenerated, OfficeObjectId, UserId, ClientIP, ModifiedProperties
```


# File upload

We can use this queries for "New executable via Office FileUploaded Operation" incidents

From the alert, we observe:
```
project UserId, ClientIP, Site_Url, SiteUrlUserFolder, FileNames
```

```kql
// File Activity
OfficeActivity
| where SourceFileName has "filename"
| where UserId has "user.name"
| order by TimeGenerated asc
```


Getting the file hash
```kql
DeviceFileEvents
| where FileName has "filename"
| where InitiatingProcessAccountUpn has "user.name"
// If the user search above doesnt work, try with the sharepoint url / username
//| where FileOriginUrl has "user_name"
| order by TimeGenerated asc
| project SHA256
```

Observe the `DeviceId` and `DeviceName` and check for activities on those.


If the file is downloaded from some site:
InitiatingProcessFileName
msedge.exe




Process executions by filename


```
search "FILENAME"
| where $table == "DeviceProcessEvents"
| where TimeGenerated between(datetime("2023-01-03 15:00:00") .. datetime("2023-01-03 19:00:00"))
| project TimeGenerated, DeviceName, InitiatingProcessAccountName, InitiatingProcessFolderPath, InitiatingProcessParentFileName, ProcessCommandLine
| order by TimeGenerated asc
```
