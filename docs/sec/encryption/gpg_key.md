---
sidebar_position: 1
id: gpg_key
title: GPG Key
slug: gpg/key
tags:
    - gpg
    - encryption
---
# GPG Key

## Generating

1. Generate a GPGKey, following the steps given by the command outputs.
```
gpg --full-gen-key
```

## Public, private and fingerprint

1. List the keys with:
```
gpg --list-secret-keys --keyid-format LONG
```

Or simply
```
gpg -aK
```

2. Get the fingerprint
```
GPGKEY_FP=$(gpg --list-secret-keys --with-colons --fingerprint | awk -F: '$1 == "fpr" {print $10;}' | head -1)
```

3. Get the full private key
```
GPGKEY_PK=$(gpg --export-secret-keys ${GPGKEY_FP} | base64 -w 0)
```

4. Get the public key
```
GPGKEY_PB=$(gpg --armor --export ${GPGKEY_FP})
```

## Encrypting

While working at some corporative projects, you'll probably handle CONFIDENTIAL INFORMATION.

Therefore, be sure to properly protect it.

```
gpg -aesr gmail@osouza.de -o <file.asc> <file.txt>
```

It's a shortcut of:
```
gpg --armor --encrypt --sign --recipient gmail@osouza.de --output <file.asc> <file.txt>
```

### Symmetric
You can encrypt a file using a symmetric method `gpg -c`. But its not safe.

If you want to use it anyway, do in a safer approach

```
gpg -c --s2k-cipher-algo AES256 --s2k-digest-algo SHA512 --s2k-count 65536 <file>
```


## Decrypting

// TODO

### Edit GPG encrypted files on-the-fly
Check [this article](../../dev/vim.md#gpg-encrypted-files).


## Signing


## Checking 

// TODO


## Key Agent

// TODO

Maybe [here](https://wiki.archlinux.org/title/GnuPG#gpg-agent) we can see something...


## References

  - [ArchWiki - GnuPG](https://wiki.archlinux.org/title/GnuPG#Encrypt_and_decrypt)

