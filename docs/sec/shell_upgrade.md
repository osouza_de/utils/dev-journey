# Shell upgrade

Grab some shell from [this list](https://book.hacktricks.xyz/generic-methodologies-and-resources/shells/linux#reverse-shell)

**Note**: I found out that zsh messes with the raw stty... Therefore do it with other shell than `zsh`, for instance: `bash`!

If you've made a reverse shell like

```
while true; do nc -lvvp 4747; done;
```

You probably got a shitty shell. You can try to upgrade it with Python:

With the shell opened, run (I'm assuming the system you owned has python):

```
python3 -c 'import pty; pty.spawn("/bin/bash")'
```
**Note:** not every server have `bash` installed, so the above command can throw an error. Try `sh` instead...

This way you'll have a nice shell already


1. At host, grab some info:
```
echo $TERM
xterm-256color

stty -a
speed 38400 baud; rows 17; columns 168; line = 0;
```

Now you know the terminal type and size.

2. Now, go to the victim shell and put it to the background with `CTRL+Z`

3. and run

```
stty raw -echo; fg
```

4. You will go back to the shell. Everything will look buggy.
Type `reset` and press enter to get it back to the normal
```
reset
```

5. Then run
```
export TERM=xterm-256color
export SHELL=bash
stty rows 17 cols 168
```

et voilá!

## Refs

  - [ropnop blog - Upgrading Simple Shells](https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/)
