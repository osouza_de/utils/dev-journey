---
id: snippets
title: Snippets
tags:
    - infosec
    - snippets
    - android
---

# Android Snippets

## CPU architecture
In order to find out the Android build details:
```
adb shell getprop ro.product.cpu.abilist
```

## Listing packages
  - To list every installed package
```
adb shell pm list packages -f
```

  - User-installed packages
```
adb shell pm list packages -f -3
```

  - From a `.apk` file
```
apk="rootbeer-sample_0.9.apk" && aapt dump badging ${apk} | grep package:\ name
```
```
package: name='com.scottyab.rootbeer.sample' versionCode='10' versionName='0.9' compileSdkVersion='30' compileSdkVersionCodename='11'
```
Replace `rootbeer-sample_0.9.apk` accordingly


## Listing package actitivies
```
apk="com.android.egg" && adb shell dumpsys package | grep -i $apk | grep Activity
```
```
        b4606be com.android.egg/.neko.NekoActivationActivity
```
Replace `com.android.egg` accordingly

## Starting activity with debug option
```
act="com.android.egg/.neko.NekoActivationActivity" && adb shell am start -n $act -D
```

## Proxy settings

  - Resetting
```
adb shell settings put global http_proxy none
```

  - Setting
```
adb shell settings put global http_proxy ${host_ip}:8080
```