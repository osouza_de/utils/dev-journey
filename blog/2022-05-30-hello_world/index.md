---
date: 2022-05-30T10:00
slug: hello-world
title: Hello World
authors: [osouza.de]
tags: [life, history]
---

Hello, friend.

I'll try to give you a glimpse of the history that drew me into the I.T. world.

## Child's play

When I was a child, I used to disassemble my electronic toys to take out the motors and gears. For what? To understand how that works and make it work differently.

I was astounded when I managed to better understand how a brushed DC electric motor works. (btw, you can learn it [here](https://electronics.howstuffworks.com/motor.htm))

<details><summary>DC Motors</summary>
<p>

![DC motor](./dc_motor.jpg)
![DC motor components](./dc_motor_components.jpg)

</p>
</details>

The problem is that I almost never managed to make those toys properly work again. :joy:


## Palmtop

My dad used to be a salesman (beverages, coffee, flour, ...) and the companies he worked for provided a handheld device (usually a PalmTop). It was right there that my curiosity began:

Usually, he brought his device home, and my brothers and I always asked to play with it. My dad allowed it with the condition that "do not break it!"

<details><summary>PalmTop</summary>
<p>

![PalmTop](./palmtop.jpg)

</p>
</details>

We used to make some drawings and play solitaire. But... I've learned that there were some "strange" files on it, system stuff. It was a Windows-powered device.
It took me a while to figure out that it was a computer, but it doesn't matter; my curiosity had been well fertilized.

**P.S.** we never broke any of our father's devices.


## Desktop

Until 2007, I had very little contact with home-use computers (desktops, to be more precise). Computers back in those days were way too expensive and my family had enough for our wellbeing. They also weren't that necessary for common household use.

A friend of mine helped me create my first email account (which I still own). It was a Hotmail. I was a piece of work... I had a typewriter with which I made kind of a business card with my email and distributed it to all of my friends.

Then I created my Orkut page and started using the computer as a "regular" person.


### Script Kiddie

This is where "hacking / social engineering" started popping out...

I started making friends on Orkut and I tried to reach them via MSN Messenger. But some of them didn't have an MSN account with the same Orkut e-mail. So I realized that Orkut didn't check if the person owned it.

Once, I had to recover my Orkut account, and for that, a URL was sent to my e-mail.

I put it all together and thought: "What if I create an e-mail for every unexistent one and request an Orkut password recovery?" 

*et voilá!*

But I realized that it was a very time-consuming task to manually get the Orkut e-mail and check if the it existed. So I found a webpage who managed that for me.

A lot of Orkut accounts were lost in that (F). I deeply regret it.


### Coding

My family managed to buy a reasonable desktop computer. But we didn't have an internet connection. My elder brother used to go to the Culture Centre to use their computers and internet connection to download some stuff and bring it home on his pendrive.

Then I got better contact with the internet in the 7th grade of middle school. A friend and school colleague had a cybercafe, and I always used to do some school work with him. I usually did all the jobs as he allowed me to stay at the cybercafe free of charge for the rest of the day!

I used to print a lot of tutorials. I went online, got some PDFs and went to the print shop to put them on paper.


#### Open Tibia Server

There I also met Tibia, learned how to play with them, and so on. One day, I saw them setting up their own private Tibia server (OpenTibia Project). I was amazed and asked them to share the software with me.

I put the server on my MP3 player (yes, it was the only thumbdrive I had) and set up my own private server at home, offline.

Then the stuff evolved: I got more sophisticated software with a website and database. I learned [LUA](https://www.lua.org/), [PHP](https://www.php.net/), and [MySQL](https://www.mysql.com) as a result of this. 

I made some online friends and also met some of the best Brazilian Open Tibia Server (I don't recall the name of it). I was very lucky to find a **PHP SHELL** in the server website. With that I started chaos. Made myself a Game Master (Tibia players used to call it GOD) and devasted the server. Spawned a lot of *Morgaroths* in front of the Thais DP and much more...

So I waited until the server's owner to login and scared him. Took his access and stuff like that. After a conversation, I was kindaof invited to join his team. There I learned much more.

The mess was so extreme that a reset wasn't enough. They had to change the server name!


#### Conquer Online

My elder brother had a Conquer Online installer. My other brother and I would sit and stare at the splash screen, imagining what the game would be like. Until the day **we finally got an internet connection** and started playing!

It didn't take too long for me to start my own Conquer Online Private Server. I also made a lot of friends from my city with it.


### Cybercafe job

I got my first job in tech at a cool cybercafe. There I managed to take care of the computers. I also had more time to learn and kind of got paid for it.


### Hardware repairs

My elder brother and I learned how to fix our own computer at home as it was expensive to pay a repair shop to do so, and we usually managed to break it. Besides that, we wanted to make the computer work in our way.

I remember the first time we tried to reinstall Windows on our machine... It erased everything because it was one of the CDs from the manufacturer and not a regular installation media from Microsoft. We were eating soup with our parents at the table close to the computer when the installation finished. We stared at each other's faces and dashed to the computer to see that **all of our data** (including family's pics) had been completely wiped out. :grimacing:

The experience came and I started offering computer repair services at my high school and also to the neighbors and some acquaintances. Some of my teachers used to hire me to fix their computers and so on. I never ever hacked any of them. **I swear**. My father **always** offered my services to their coworkers, and I started fixing mobile phones as well.


## Laptop

My mother gifted me with a laptop. JUST FOR MYSELF! It was a great step changer in my life, because I didn't have to share and wait my time to access the internet and do my stuff.

With this, I had more time and more privacy, so I started a few projects and went to the "DeepWeb", where I learned a LOT of cool stuff.


## High School

At high school, we had a nice computer room. At the beginning, it was Windows machines.


### Fighting censorship

Facebook was forbidden by the director, but the students didn't care. Until they started blocking it. As the good rebel I was, I didn't like that and started to investigate what they did. I saw that they didn't use Proxy, so it would be easier. I figured out that someone had set something like:
```
file C:\Windows\System32\Drivers\etc\hosts

127.0.0.1 facebook.com
```

I started spreading the word! I taught everybody how to bypass the censorship and freely browse the internet back!

But something strange started to happen: some of the students who used the school lab to access their Facebook began to lose their accounts.

Then I investigated what was going on. TADA! Found **keyloggers** on every lab computer. Here I must confess that I owned some accounts of people who I didn't like. But I didn't own the keyloggers; I just took advantage. After that, I uninstalled every keylogger.

The school management then gave up on trying to censor the internet :sunglasses:


### Being a 4ssh0le

A government program distributed Linux computers to our school. What a nice stuff! The issue is that every machine had SSH enabled by default. It doesn't took too long for me to get root.

When my school class used the computer lab, I used to log into every machine via SSH and mess around without my colleagues' knowledge (and permission). Once there, I killed some processes, like the internet browser or text editor, or even worse, made some files (hours of handwriting) simply disappear. I was never caught.

There's a case that happened there that I'm not sure if I can disclose here. So maybe in the future...


### Being a snitch

Back in those days, there was a website called "AskFM". It was a page where you received some questions from your followers / visitors and answered them publicly.

Some guy had a brilliant idea to start an anonymous defamation page. People went there and asked about some students, and the answers were usually acidic, toxic and defamatory.

The victims became upset and started a manhunt. One guy reached me and offered me BRL 50,00 if I managed to get the anon. I accepted the offer, not for the money, but for the challenge itself.

I will not disclose here how I did it, but I went to confront the guy (he was almost a friend), and he, knowing that someone paid me to investigate, confessed to me and almost begged to keep it secret. I didn't.

The end of the story is that he was almost lynched by the whole school, had to call the police to leave, and was bullied by everyone. Also, I almost got beat up by his friend who claimed to be part of the joke. And... I'm still waiting for my fifty bucks.


### ShareIT facebook school group

As I was learning the hacking philosophy, I saw that it would be nice if I could:
  - learn how to code
  - share my knowledge

I came up with a very efficient way to accomplish both, and I decided to start a **study group** at my school because you learn as you teach, and learning means earning!

But, unfortunately, that idea never evolved. :sweat:

<details><summary>ShareIT Facebook group</summary>
<p>

![ShareIT Facebook group](./facebook_shareit.jpg)

</p>
</details>


## furthermore...

The rest of my history I'll publish in a few days. Don't lose it!
